﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;

    // 타워오브젝트 세로라인 위치.
    public static readonly float[] gridY = new float[4] { 2f, 2.9f, 3.8f, 4.7f };

  //  public DataController dataController;

    public GameObject restartBtn;
    public GameObject towerFab;
    public GameObject popupGameOver;
    public Text scoreText, bestText, levelText;  //gameOverText;

    public static bool done;
    bool spawnWaiting;
    bool go = true;
    bool winner;
    bool gameOver;

    private bool restart;
    

    public static TowerTile[,] grid = new TowerTile[4, 4];
    

    void Start()
    {
        Instance = this;
        gameOver = false;
        restart = false;

      
        Cursor.visible = false;

        DataController.Instance.gameData.Best = PlayerPrefs.GetInt("High Score");
        System.Array.Clear(grid, 0, grid.Length);
        done = false;
      

        // scoreText.transform.position = Camera.main.WorldToViewportPoint(new Vector3(0f, -22.5f, 0f));
        scoreText.GetComponent<Text>().text = "" + DataController.Instance.gameData.Score;
      //  bestText.transform.position = Camera.main.WorldToViewportPoint(new Vector3(0f, -22f, 40f));
        bestText.GetComponent<Text>().text = "" + DataController.Instance.gameData.Best;   


        Spawn();
        Spawn();

        

       // OnGameOver();

    }

    public void Restart()
    {
        gameOver = false;
        restart = false;
        DataController.Instance.gameData.Score = 0;

        Cursor.visible = false;

        DataController.Instance.gameData.Best = PlayerPrefs.GetInt("High Score");
        System.Array.Clear(grid, 0, grid.Length);
        done = false;


        // scoreText.transform.position = Camera.main.WorldToViewportPoint(new Vector3(0f, -22.5f, 0f));
        scoreText.GetComponent<Text>().text = "" + DataController.Instance.gameData.Score;
        //  bestText.transform.position = Camera.main.WorldToViewportPoint(new Vector3(0f, -22f, 40f));
        bestText.GetComponent<Text>().text = "" + DataController.Instance.gameData.Best;

        GameObject[] objs = GameObject.FindGameObjectsWithTag("Tower");
        foreach(GameObject obj in objs)
        {
            Destroy(obj);
        }

        Spawn();
        Spawn();
    }


    public void OnGameOver()
    {
        //게임오버시 게임오버팝업창 뜸
        //하단의 텍스트 자리에 타이틀과 부제 텍스트 자유 변경 가능.
        DialogDataAlert alert = new DialogDataAlert("GAME OVER", "You looseeeeeee", delegate () {
        });
        DialogManager.Instance.Push(alert);

    }
    
    
    IEnumerator TryToSolveMyProblem()
    {
        go = false;
        yield return new WaitForSeconds(0.33f);
        go = true;
    }


      //public float lastDone = Time.time;

    void Update()
    {       
        

     //  if (Input.GetKey(KeyCode.Escape))
     //       Application.Quit();

         done = true;
        /*
        GameObject[] towers = GameObject.FindGameObjectsWithTag("Tower");
        foreach(GameObject tower in towers)
        {
            if(tower.GetComponent<TowerTile>().done == false)
            {
                done = false;
                lastDone = Time.time;
            }
        }
        */

        if (done && spawnWaiting)
            Spawn();

//#if UNITY_WEBGL

        //타워 오브젝트 플레이??합쳐지는 시간 설정하는 곳    ---------->요기.
        if (done & !gameOver && go == true)
        {
            if (Input.GetKey(KeyCode.UpArrow))
                Move(0);
            if (Input.GetKey(KeyCode.DownArrow))
                Move(1);
            if (Input.GetKey(KeyCode.LeftArrow))
                Move(2);
            if (Input.GetKey(KeyCode.RightArrow))
                Move(3);
        }
//#endif
        if (winner)
        {
            if(DataController.Instance.gameData.Score > DataController.Instance.gameData.Best)
            {
                DataController.Instance.gameData.Best = DataController.Instance.gameData.Score;
                PlayerPrefs.SetInt("High Score", DataController.Instance.gameData.Best);
            }


        }


        if (gameOver)
        {
            //  gameOverText.transform.position = Camera.main.WorldToViewportPoint(new Vector3(4.8f, 7.6f, 0f));
            // gameOverText.GetComponent<Text>().text = "Game Over"; 이건 게임오버시 게임오버 텍스트가 뜸
         
                                                                   
            if (DataController.Instance.gameData.Score > DataController.Instance.gameData.Best)
            {
                DataController.Instance.gameData.Best = DataController.Instance.gameData.Score;
                PlayerPrefs.SetInt("High Score", DataController.Instance.gameData.Best);
            }
          
         //   if (Input.GetKeyDown("space"))
          //      Application.LoadLevel(0); 
        }
    }

 /*   void OnGUI()
    {
        GUIStyle myStyle = new GUIStyle();
        if (gameOver == true || winner == true)
        {
            if (GUI.Button(new Rect(Screen.width / 2 - Screen.width / 8, Screen.height / 2 - Screen.height / 8, Screen.width / 4, Screen.height / 4), restartBtn, myStyle ))
                Application.LoadLevel(0);

        }

    }     */

    public void Move(int dir)
    {

        if (gameOver)
            return;
        if (!done)
            return;

        StartCoroutine(TryToSolveMyProblem());
       
        done = false;
        Vector2 vector = Vector2.zero;
        bool moved = false;
       
        int[] xArray = { 0, 1, 2, 3 };
        int[] yArray = { 0, 1, 2, 3 };
       
        switch (dir)
        {
            case 0:
                vector = Vector2.up;
                System.Array.Reverse(yArray);
                break;
            case 1:
                vector = -Vector2.up;
                break;
            case 2:
                vector = -Vector2.right;
                break;
            case 3:
                vector = Vector2.right;
                System.Array.Reverse(xArray);
                break;
        }

        foreach (int x in xArray)
        {

            foreach (int y in yArray)
            {
                if (grid[x, y] != null)
                {
                    grid[x, y].combined = false;
                    Vector2 cell;
                    Vector2 next = new Vector2(x, y);
                    do
                    {
                        cell = next;
                        next = new Vector2(cell.x + vector.x, cell.y + vector.y);
                    } while (isInArea(next) && grid[Mathf.RoundToInt(next.x), Mathf.RoundToInt(next.y)] == null);

                    int nx = Mathf.RoundToInt(next.x); int ny = Mathf.RoundToInt(next.y);
                    int cx = Mathf.RoundToInt(cell.x); int cy = Mathf.RoundToInt(cell.y);
                   
                    if (isInArea(next) && !grid[nx, ny].combined && grid[nx, ny].tileValue == grid[x, y].tileValue)
                    {
                        DataController.Instance.gameData.Score += grid[x, y].tileValue * 2; 
                        scoreText.GetComponent<Text>().text = "" + DataController.Instance.gameData.Score;
                        grid[x, y].Move(nx, ny); 
                        moved = true;
                        if ((grid[nx, ny].tileValue * 2) == 2048)
                        {
                           
                            winner = true;
                        }
                    }
                    else
                    {
                        if (grid[x, y].Move(cx, cy))
                            moved = true; 
                    }
                }
            }
        }
        if (moved)
        { 
            spawnWaiting = true;
        }
        else
        {
            moved = false;
            for (int x = 0; x <= 3; x++)
            {
                for (int y = 0; y <= 3; y++)
                {
                    if (grid[x, y] == null)
                    {
                        moved = true;
                    }
                    else
                    {
                        for (int z = 0; z <= 3; z++)
                        {
                            Vector2 Vtor = Vector2.zero;
                            switch (z)
                            { 
                                case 0:
                                    Vtor = Vector2.up;
                                    break;
                                case 1:
                                    Vtor = -Vector2.up;
                                    break;
                                case 2:
                                    Vtor = Vector2.right;
                                    break;
                                case 3:
                                    Vtor = -Vector2.right;
                                    break;
                            }
                            if (isInArea(Vtor + new Vector2(x, y)) &&
                                grid[x + Mathf.RoundToInt(Vtor.x), y + Mathf.RoundToInt(Vtor.y)] != null &&
                                grid[x, y].tileValue == grid[x + Mathf.RoundToInt(Vtor.x), y + Mathf.RoundToInt(Vtor.y)].tileValue)
                                moved = true;
                        }
                    }
                }
            }
            
            if (!moved)
            {
                gameOver = true;

                OnGameOver();
            }
        }
        done = true;
    }

    bool isInArea(Vector2 vec)
    {
        return 0 <= vec.x && vec.x <= 3 && 0 <= vec.y && vec.y <= 3;
    }

    void Spawn()
    {

        spawnWaiting = false;
        bool oc = true;
        int xx = 0;
        int yy = 0;
      
        while (oc)
        {
            xx = Random.Range(0, 4);
            yy = Random.Range(0, 4);
           
            if (grid [xx, yy] == null) oc = false;
        }
       
        int startValue = 4; 
        if (Random.value < 0.88f)
            startValue = 2; 

        GameObject tempTile = (GameObject)Instantiate(towerFab, gridToWorld(xx, yy), Quaternion.Euler(0, 0, 45)); //카메라 각도 조절을 할뚜이따.
        grid [xx, yy] = tempTile.GetComponent<TowerTile>();
        grid [xx, yy].tileValue = startValue;
    }

// 타워 오브젝트 가로 간격
    public static Vector2 gridToWorld(int x, int y)
    {
        return new Vector2(0.9f * x + 0.9f, gridY[y]);
    
    }

    public static Vector2 worldToGrid(float x, float y)
    {
        for (int i = 0; i <= 3; i++)
        {
            if (gridY[i] == y) y = i;
        }
        return new Vector2((x - 0.9f) / 0.9f, y);
    }

}