﻿#if UNITY_EDITOR 
using UnityEngine;
#endif 
using System.Collections;
using UnityEngine.UI;

public class DialogControllerTower : DialogController
{
    //dialog alert 는 강사님이 만들어준 팝업창 띄우는 로직.
    //텍스트 내용 내마음대로 바꿀 수 있음.
	public Text LabelTitle;
	public Text LabelMessage;

	DialogDataTower Data {
		get;
		set;
	}

	public override void Awake ()
	{
		base.Awake ();
	}

	public override void Start ()
	{
		base.Start ();

		DialogManager.Instance.Regist( DialogType.Tower, this );
	}

    public override void Build(DialogData data)
    {
		base.Build(data);
		
		if( ! (data is DialogDataTower) ) {
			//Debug.LogError("Invalid dialog data!");
			return;
		}
		
		Data = data as DialogDataTower;
		LabelTitle.text = Data.Title;
		LabelMessage.text = Data.Message;
    }

    public void OnClickOK()
    {
        // calls child's callback
        if (Data!=null && Data.Callback != null)
            Data.Callback();

		DialogManager.Instance.Pop();
    }
}
