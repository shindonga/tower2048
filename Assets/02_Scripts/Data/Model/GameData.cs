﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData {

    public int Score;
    public int Best;

    public int Exp { get; internal set; }
}
