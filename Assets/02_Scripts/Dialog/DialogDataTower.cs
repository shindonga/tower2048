﻿using System;

public class DialogDataTower : DialogData 
{
	public string Title { 
		get; 
		private set; 
	}

	public string Message { 
		get; 
		private set; 
	}

	public Action Callback {
		get; 
		private set; 
	}

	public DialogDataTower(string title, string message, Action callback = null)
		: base( DialogType.Tower )
	{
		this.Title = title;
		this.Message = message;
		this.Callback = callback;
	}
}
