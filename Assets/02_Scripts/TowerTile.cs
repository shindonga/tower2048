﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[RequireComponent(typeof(AudioSource))]

public class TowerTile : MonoBehaviour {

    public GameObject towerNewFab;
   // public AudioClip FX;


    public int tileValue;
    public bool combined;

    Vector2 movePosition;
    bool combine;
    TowerTile cTile;
    bool grow;
  //  public bool done = false;


	void Start () {

        movePosition = transform.position;
        Change (tileValue);
	}

    void Update()
    {
        if (transform.position != new Vector3(movePosition.x, movePosition.y, 0f))
        {
            GameManager.done = false;
           // done = false;            
            transform.position = Vector3.MoveTowards(transform.position, movePosition, 35 * Time.deltaTime);
        }
        else
        {
            GameManager.done = true;
            //done = true;

            if (combine)
            {
                Change(tileValue * 2);
                combine = false;
                grow = true;                
                Destroy(cTile.gameObject);

              //  GetComponent<AudioSource>().PlayOneShot(FX, 1.0f);

                GameManager.done = true;
                // done = true;
            }
        }

  /*      if (transform.localScale.x != 1 && !grow)
            transform.localScale = Vector3.MoveTowards(transform.localScale, new Vector3(1f, 1f, 1f), 500 * Time.deltaTime);
        if (grow)
        {
            GameManager.done = false;
            transform.localScale = Vector3.MoveTowards(transform.localScale, new Vector3(1f, 1f, 1f), 500 * Time.deltaTime);
            if (transform.localScale == new Vector3(1f, 1f, 1f))
                grow = false;
        }
        else
            GameManager.done = true;       */

    }

    void Change (int newValue)
    {
        tileValue = newValue;

        if(newValue == 2)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower00");
        }

        if (newValue == 4)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower01");
        }

        if (newValue == 8)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower02");
        }

        if (newValue == 16)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower03");
        }

        if (newValue == 32)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower04");
        }

        if (newValue == 64)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower05");
        }

        if (newValue == 128)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower06");
        }

        if (newValue == 256)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower07");
        }

        if (newValue == 512)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower08");
        }

        if (newValue == 1024)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower09");
        }

        if (newValue == 2048)
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Towers/Tower10");
        }
    }

    public bool Move(int x, int y)
    {

        movePosition = GameManager.gridToWorld(x, y);

        if (transform.position != (Vector3)movePosition)
        {
            if (GameManager.grid[x, y] != null)
            {
                combine = true;
                combined = true;
                cTile = GameManager.grid[x, y];
                GameManager.grid[x, y] = null;
            }

            GameManager.grid[x, y] = GetComponent<TowerTile>();
            GameManager.grid[Mathf.RoundToInt(GameManager.worldToGrid(transform.position.x, transform.position.y).x),
                Mathf.RoundToInt(GameManager.worldToGrid(transform.position.x, transform.position.y).y)] = null;
            return true;

        }
        else
            return false;
    }

}
